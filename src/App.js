import React, { Component } from 'react';
import './App.css';
import { connect } from 'react-redux';
import { DISLIKE, LIKE } from './const/actionTypes';
import Posts from './componets/Posts'

class App extends Component {

  constructor(props) {
    super(props)
    this.select = this.select.bind(this);
  }
  select(event) {
    console.log(event.target.value);
    let data = event.target.value;
    this.props.select(data);

  }

  render() {

    return (
      <div className="main-container">
        <div >
          <form>
            <select onChange={this.select} class="custom-select" id="inputGroupSelect01">
              <option selected>Select type</option>
              <option value="TEXT">Text only</option>
              <option value="IMAGE">Image only</option>
              <option value="BOTH">Both</option>
              <option value="NONE">None</option>
            </select>
          </form>
        </div>
        <div className="Posts-div">
          <Posts />
        </div>
      </div>
    );
  }
}


const mapStateToProps = state => {
  return {

  }
}
const mapDispatchToProps = dispatch => {
  return {
    select: data => dispatch({ type: data, }),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

