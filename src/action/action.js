import {
    TEXT,
    IMAGE,
    BOTH,
    NONE,
    LIKE,
    DISLIKE,
    REPLY
} from "../const/actionTypes";



export const selectText = () => ({ type: TEXT });
export const selectImage = () => ({ type: IMAGE });
export const selectBoth = () => ({ type: BOTH });
export const selectNone = () => ({ type: NONE });
export const like = () => ({ type: LIKE });
export const disLike = () => ({ type: DISLIKE });
export const reply = (comment) => ({ type: REPLY, payload: comment });