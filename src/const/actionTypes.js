export const TEXT = 'TEXT';
export const IMAGE = 'IMAGE';
export const BOTH = 'BOTH';
export const NONE = 'NONE';
export const LIKE = 'LIKE';
export const DISLIKE = 'DISLIKE';
export const COMMENT = 'COMMENT';
export const REPLY = 'REPLY';

