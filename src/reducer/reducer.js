import {
    TEXT,
    IMAGE,
    BOTH,
    NONE,
    LIKE,
    DISLIKE,
    REPLY,
} from "../const/actionTypes";


const initialState = {

    displayInfo: {
        isImage: true,
        isText: true,
    },
    content: [
        {

            item_description: 'This is a text only item',
            image: 'https://cdn.pixabay.com/photo/2017/07/19/21/57/idea-2520520_640.jpg',
            likes: 10,
            comments: [
                {
                    comment: 'This is a comment',
                    created_at: 'Fri May 11 2018 19: 17: 43 GMT + 0530(IST) '
                },
                {
                    comment: 'This is another comment',
                    created_at: 'Fri May 10 2018 15: 23: 36 GMT + 0530(IST) '
                }
            ]
        },
        {

            item_description: 'This is an item with an image',
            image: 'https://cdn.pixabay.com/photo/2017/07/19/21/57/idea-2520520_640.jpg',
            likes: 20,
            comments: [
                {
                    comment: 'This is a comment',
                    created_at: 'Fri May 11 2018 19: 17: 43 GMT + 0530(IST) '
                },
                {
                    comment: 'This is another comment',
                    created_at: 'Fri May 10 2018 15: 23: 36 GMT + 0530(IST) '
                }
            ]
        }
    ]
}

// export const selectText = () => ({ type: TEXT });
// export const selectImage = () => ({ type: IMAGE });
// export const selectBoth = () => ({ type: BOTH });
// export const selectNone = () => ({ type: NONE });
// export const like = () => ({ type: LIKE });
// export const disLike = () => ({ type: DISLIKE });
// export const comment = (comments) => ({ type: COMMENT, payload: comments });



const reducer = (state = initialState, action) => {
    let displayInfoCopy;
    let displayInfo;
    let id, newState, newContent;
    // console.log(action, "in reducer");
    switch (action.type) {

        case TEXT:
            displayInfo = state[displayInfo];
            displayInfoCopy = Object.assign({}, displayInfo, { isImage: false }, { isText: true });
            return Object.assign({}, state, { displayInfo: displayInfoCopy });

        case IMAGE:
            displayInfo = state[displayInfo];
            displayInfoCopy = Object.assign({}, displayInfo, { isImage: true }, { isText: false });
            return Object.assign({}, state, { displayInfo: displayInfoCopy });

        case BOTH:
            displayInfo = state[displayInfo];
            displayInfoCopy = Object.assign({}, displayInfo, { isImage: true }, { isText: true });
            return Object.assign({}, state, { displayInfo: displayInfoCopy });

        case NONE:
            displayInfo = state[displayInfo];
            displayInfoCopy = Object.assign({}, displayInfo, { isImage: false }, { isText: false });
            return Object.assign({}, state, { displayInfo: displayInfoCopy });
        case LIKE:
            id = parseInt(action.payload);
            newState = Object.assign({}, state);
            newContent = newState.content.map((post, index) => {
                if (index === id) {
                    post.likes++;
                    return post
                }
                else {
                    return post;
                }

            })
            return Object.assign({}, state, { content: newContent });
        case DISLIKE:
            id = parseInt(action.payload);
            newState = Object.assign({}, state);
            newContent = newState.content.map((post, index) => {
                if (index === id) {
                    post.likes--;
                    return post
                }
                else {
                    return post;
                }

            })
            return Object.assign({}, state, { content: newContent });
        case REPLY:
            id = action.payload.id;
            let comments = action.payload.comments
            newState = Object.assign({}, state);
            newContent = newState.content.map((post, index) => {
                if (index === id) {

                    return Object.assign({}, post, { 'comments': comments })
                }
                else {
                    return post;
                }

            })
            return Object.assign({}, state, { content: newContent })
        default:
            return state;
    }
};
export default reducer;

