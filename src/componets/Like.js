import React, { Fragment } from 'react';

const Like = ({ id, like, likes }) => {
    return (
        <Fragment>

            <button type="button" class="btn badge-dark  btn-sm"
                onClick={() => like(id)}
            >
                likes <span class="badge badge-dark">{likes}</span>
            </button>
        </Fragment>
    )
}

export default Like;