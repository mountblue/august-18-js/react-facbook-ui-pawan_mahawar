import React, { Fragment } from 'react';

const Like = ({ id, disLike, likes }) => {
    return (
        <Fragment>
            <button type="button" class="btn badge-dark  btn-sm"
                onClick={() => disLike(id)}
            >
                DisLike <span class="badge badge-dark">{likes}</span>
            </button>
        </Fragment>

    )
}

export default Like;