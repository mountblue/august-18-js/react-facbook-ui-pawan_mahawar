import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PostImage from './PostImage';
import ItemDescription from './ItemDescription';
import Likes from './Like';
import DisLikes from './DisLike'
import Comments from './Comments'
import { DISLIKE, LIKE, REPLY } from '../const/actionTypes'

class Posts extends Component {
    constructor(props) {
        super(props)
        this.state = {
            reply: ''
        }

        this.like = this.like.bind(this);
        this.disLike = this.disLike.bind(this);
    }

    disLike(e) {
        let id = e.target.innerHTML
        this.props.disLike(id);
    }

    like(e) {
        console.log(e.target.innerHTML)
        let id = e.target.innerHTML
        this.props.like(id);
    }


    render() {

        let postList;
        let { posts, displayInfo } = this.props;
        let { isImage, isText } = displayInfo;
        postList = posts.map((post, id) => {
            return (<div className="card">
                {
                    isText ? (<ItemDescription description={post.item_description} id={id} />
                    ) : (<Fragment>
                        {null}
                    </Fragment>)
                }
                {
                    isImage ? (<PostImage image={post.image} id={id} />
                    ) : (
                            <Fragment>
                                {null}
                            </Fragment>
                        )
                }
                {
                    (isImage || isText) ? (
                        <Fragment>
                            <div className="like-dislike">
                                <Likes likes={post.likes}
                                    id={id}
                                    like={this.props.like}
                                />
                                <DisLikes likes={post.likes}
                                    id={id}
                                    disLike={this.props.disLike}
                                />
                            </div>
                            <Comments comments={post.comments}
                                replyToPost={this.props.replyToPost}
                                id={id} />
                        </Fragment>
                    ) : (
                            <Fragment>{null}</Fragment>
                        )
                }

            </div>)

        })

        return (<Fragment>{postList}</Fragment>);
    }
}

const mapStateToProps = state => {
    return {
        posts: state.content,
        displayInfo: state.displayInfo
    }
}
const mapDispatchToProps = dispatch => {
    return {
        select: data => dispatch({ type: data, }),
        disLike: id => dispatch({ type: DISLIKE, payload: id }),
        like: id => dispatch({ type: LIKE, payload: id }),
        replyToPost: commentData => dispatch({ type: REPLY, payload: commentData })
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Posts);