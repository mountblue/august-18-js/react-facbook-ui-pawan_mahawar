import React from 'react';

const ItemDescription = (props) => {
    return (
        <div class="alert alert-light " >
            <strong id={props.id}>{props.description}</strong>
        </div>
    )

}

export default ItemDescription;