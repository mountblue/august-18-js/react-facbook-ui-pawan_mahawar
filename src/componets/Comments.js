import React, { Component, Fragment } from 'react';

class Comments extends Component {
    constructor(props) {
        super(props)
        this.state = {
            reply: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {

        let reply = event.target.value;
        this.setState({
            reply
        })
    }

    handleSubmit(event) {
        let { comments, replyToPost, id } = this.props;
        event.preventDefault();
        let reply = this.state.reply;
        let created_at = new Date().toString().substr(0, 34) + "(IST)";
        let comment = { 'comment': reply, 'created_at': created_at.toString() }
        let newComments = [...comments, comment]
        let commentData = { "id": id, "comments": newComments }
        replyToPost(commentData);
        this.setState({
            reply: ''
        })
    }

    render() {
        let { comments } = this.props;
        let commentlist;

        commentlist = Object.keys(comments).map((key) => {
            return (
                <div >
                    <p className="comment-p-tag">  {comments[key].comment}</p>
                    <span className="date-span">{comments[key].created_at}</span>
                    <hr />
                </div>
            )
        })

        return (
            <div className="comment-div">
                <Fragment>
                    {commentlist}
                </Fragment>
                <form onSubmit={this.handleSubmit}>
                    <input type="text" name="userid" placeholder="Reply to comment..."
                        value={this.state.reply}
                        onChange={this.handleChange} /><br />
                </form >
            </div>
        )

    }
}

export default Comments;


