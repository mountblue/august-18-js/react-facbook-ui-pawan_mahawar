
import Img from 'react-image';
import React from 'react';

const PostImage = (props) => {
    return (
        <p className="img-class">
            <Img id={props.id} src={props.image} alt="" />
        </p>
    )
}

export default PostImage;